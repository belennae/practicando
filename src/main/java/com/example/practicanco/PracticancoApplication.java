package com.example.practicanco;

import com.example.practicanco.models.MetodosReferenciados;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class PracticancoApplication {

    public static void main(String[] args) {

        //MetodosReferenciados.Methods.sayStaticHello();
        MetodosReferenciados.Methods.pruebita();
        //MetodosReferenciados.Methods.home(args);
        //MetodosReferenciados.Methods.interfazFuncional();
        SpringApplication.run(PracticancoApplication.class, args);
    }

}
