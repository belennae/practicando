package com.example.practicanco.models;


import com.example.practicanco.bean.Hello;
import com.example.practicanco.bean.Persona;
import com.example.practicanco.interfazFuncional.IHello;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;


public class MetodosReferenciados {

    public static class Methods {
        public static void sayStaticHello() {
            System.out.println("hola soy una clase statica");
        }

        public static void interfazFuncional(){

            List<Persona> lista = new ArrayList<>();
            Persona p1 = new Persona("pepe", "perez", 20);
            Persona p2 = new Persona("angel", "sanchez", 30);
            Persona p3 = new Persona("pepe", "blanco", 40);
            lista.add(p1);
            lista.add(p2);
            lista.add(p3);


           /*Predicate<Persona> predicadoNombre = new Predicate<Persona>() {
                @Override
                public boolean test(Persona p) {
                    return p.getNombre().equals("pepe");}
            };
            lista.stream().filter(predicadoNombre).forEach(p -> System.out.println(p.getApellidos()));*/
/*
            Predicate<Persona> predicate = (a) -> {
                return a.getNombre().equals("pepe");
            };
            lista.stream().filter(predicate).forEach(p -> System.out.println(p.getApellidos()));*/


            //solo con lambda
            lista.stream()
                    .filter(p -> p.getNombre().equals("pepe"))
                    .forEach(p -> System.out.println(p.getApellidos()));



        }

        public static void pruebita(){
            List<String> lista= new ArrayList<String>();
            lista.add("Carlos");
            lista.add("Edwin");
            lista.add("Angela");

            List<String> lista2= Arrays.asList("Raquel","Monica","Alison");

          /*  lista.forEach(System.out::println);

            System.out.println("-----------------");
            lista.sort((pa,pb)->pb.compareTo(pa));
            lista.forEach(System.out::println);
*/
            System.out.println("-----------------");
            lista.stream().sorted().forEach(System.out::println);
            System.out.println("-----------------");
            lista.stream().sorted((x,y) -> y.compareTo(x)).forEach(System.out::println);
            System.out.println("-----------------");
            lista.forEach(System.out::println);


            System.out.println("-----------------");
            lista2.forEach(System.out::println);
            lista2.stream().map((x)-> x.toUpperCase()).forEach((x)->System.out.println(x));
        }
        public void sayInstanceHello() {
            System.out.println("hola soy un metodo instanciado ");
        }

        public static void home(String[] args) {

            //Referencia a un método estatico
            IHello staticRef = Methods::sayStaticHello;
            staticRef.sayHello();

            //Referencia a un método de un objeto
            Methods methods = new Methods();
            IHello instanceRef = methods::sayInstanceHello;
            instanceRef.sayHello();

            //Referencia a un método de un objeto arbitrario
            String[] names = new String[]{"Oscar", "Alex", "Maria", "Samuel", "Perla", "Fausto"};
            Arrays.sort(names, String::compareToIgnoreCase);
            System.out.println("Array ordenado " + Arrays.toString(names));

            //Referencia a un constructor
            IHello hello = Hello::new;
            hello.sayHello();
        }
    }
}
