package com.example.practicanco.bean;

import com.example.practicanco.interfazFuncional.IHello;

public class Hello implements IHello {
    private String helloMessage;
    public Hello() {
        System.out.println("Hola soy un constructor");
    }
    public void createHello(String helloMessage) {
        this.helloMessage = helloMessage;
    }
    @Override
    public void sayHello() {
        System.out.println(this.helloMessage);
    }

}
