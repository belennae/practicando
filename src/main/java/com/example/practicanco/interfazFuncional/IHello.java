package com.example.practicanco.interfazFuncional;

@FunctionalInterface
public interface IHello {
    public void sayHello();
}
